---
title: "Caracterizaão da área dos assentamentos de Palmas, através de dados georeferenciados e sofwares disponíveis na internet"
author: "Adenor Vicente Wendling"
date: "28/02/2022"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

O projeto em desenvolvimento tem dois objetivos:
#1.  Elaborar tutoriais em R, para elaborar mapas temáticos dos assentamentos São Lourenço, Paraíso do Sul e Margem do Irati, de Palmas, PR, com os seguintes capítulos:
#1. Elaborar um documento com os dados do Assentamento Sâo Lourenço, abrangendo dados topográficos, ambientais, climáticos e das características do solo. 

## Tutoriais
1. [Tutorial 1 - Extrair e manipular dados do KOBO ](tutoriais/Tutorial_1_OrganizarDadosColetaKobo.Rmd)
Durante o trabalho de identificação dos assentamentos foram coletados amostras de solos, e, através do aplicativo KOBOTOOLS, aplicado um questionário sobre a área. Tanto o ponto de coleta, quanto as informaõções do questionário foram extraídas e interpretadas através desse tutorial.

1. [Tutorial 2 - Como importar e inserir arquivos de diversas fontes](tutoriais/Tutorial_2_Baixar_Inserir_Camadas_2.Rmd)
Para elaborar o relatório com as características da área dos assentamentos, foram importandos inúmeros arquivos vetoriais e matriciais. Neste tutorial são descritos todos os passos para importar, recortar e manipular esses dados para compor os mapas temáticos da área.

1. [Tutorial 4 - Cálculos de porcentagens de arquivos raster](tutoriais/Tutorial_4_Calculos de Porcentagem.Rmd)

## Relatorios
## Mapas em Qgis
1. [Mapa com várias camadas de interesse](data/Coleta_Assentamento_SL_1.csv)
1. [Relatório do estágio com a caracterização dos assentamentos de Palmas, PR](Relatos/RelatorioEstagioII.Rmd)



# lol.git.disroot.org

