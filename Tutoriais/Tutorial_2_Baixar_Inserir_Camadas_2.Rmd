---
title: "Arquivos com informações do assentamento"
author: "Adenor Vicente Wendling"
date: "24/02/2021"
output:
  html_document:
    toc: true
    toc_float: true
    theme: united
    highlight: zenburn
    df_print: paged
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```


```{r}
library(knitr)
library(tidyverse)
library(readr)
library(kableExtra)
```

# Introdução
A elaboração de projetos no software Qgis está sendo facilitada diariamente, através da disponivilização de arquivos e banco de dados em inúmeros formatos e sites.


Um projeto deve ser o mais preciso prossível. Por isso, a fonte de dados é importante. ńúmeras bases de dados com informações reevantes estão dispinívies para uso gratuito, em bancos de dados públicos ou privados.

Antes de mais nada, entretanto, é necessário instalar o QGIS em seu PC.


# Utilização Bancos de dados para geação de mapas.
## Onde buscar os dados
Existem inúmeros sites que disponibilizam arquivos em formatos vetorial ou Raster gratuitamente (Lista 1). 


Lista 1 - Relação de sites com arquivos (banco de dados) para QGIS.
----
### IBGE
* ftp://geoftp.ibge.gov.br/
* ftp://geoftp.ibge.gov.br/cartas_e_mapas/bases_cartograficas_continuas/bc250/versao2015/Shapefile
* ftp://geoftp.ibge.gov.br/informacoes_ambientais
* https://www.ibge.gov.br/geociencias/downloads-geociencias.html
* https://mapas.ibge.gov.br/en/bases-e-referenciais/bases-cartograficas/cartas.html
* https://portaldemapas.ibge.gov.br/portal.php#mapa15666
* http://mapas.ibge.gov.br
* http://www.ppp.ibge.gov.br/ppp.htm

### Modelo digital de elevação
* https://earthexplorer.usgs.gov/
* https://www.webmapit.com.br/inpe/topodata/
* https://search.asf.alaska.edu/#/
* https://www.cnpm.embrapa.br/projetos/relevobr/download/

### Imagens de satélite
* http://earthexplorer.usgs.gov/
* https://search.remotepixel.ca/
* https://search.asf.alaska.edu/
* http://www.dgi.inpe.br/CDSR/
* http://www2.dgi.inpe.br/catalogo/explore
* https://www.usgs.gov/products/data-and-tools/real-time-data/remote-land-sensing-andlandsat
* http://www.dgi.inpe.br/CDSR/
* https://inde.gov.br/CatalogoGeoservicos

### CAR
* http://www.car.gov.br/publico/imoveis/index

### EMBRAPA
* http://mapas.cnpm.embrapa.br/somabrasil/webgis.html
* https://www.cnpm.embrapa.br/projetos/relevobr/download/

### ANA Agência Nacional das Águas
* https://metadados.ana.gov.br/geonetwork/srv/pt/main.home
* http://hidrosat.ana.gov.br/SaibaMais/Download
* http://dadosabertos.ana.gov.br/

### Ministério do Meio Ambiente Brasil MMA
* http://mapas.mma.gov.br/i3geo/datadownload.htm

### CPRM
* http://geowebapp.cprm.gov.br/ViewerWEB/

### FUNAI
* http://www.funai.gov.br/index.php/shape

### Acervo fundiário
* http://acervofundiario.incra.gov.br/acervo/acv.php
* http://acervofundiario.incra.gov.br:8080/Conversao01/

### DNIT
* http://www.dnit.gov.br/mapas-multimodais/shapefiles
* https://portalgeo.seade.gov.br/

### IBAMA
* http://siscom.ibama.gov.br/monitora_biomas/PMDBBS%20-%20CERRADO.html
* http://mapas.mma.gov.br/mapas/aplic/probio/datadownload.htm

### INPE
* http://www.dpi.inpe.br/tccerrado/dados/2013/mosaicos/

### Dados de geologia
* https://mega.nz/folder/kThXHRLJ#j18o4gKegNzs7bkPc2ezxg
* http://www.portalgeologia.com.br/index.php/mapa/#downloads-tab
* https://www2.sgc.gov.co/ProgramasDeInvestigacion/Geociencias/Paginas/GMSA.aspx
* http://www.cprm.gov.br/publique/Gestao-Territorial/Prevencao-de-DesastresNaturais/Produtos-por-Estado---Setorizacao-de-Risco-Geologico-5390.html
* http://geosgb.cprm.gov.br/

### ArcGIS Online
* https://www.arcgis.com/home/index.html

### ArcGISHUB
* http://hub.arcgis.com/

### Base SICAR
* https://www.car.gov.br/publico/municipios/downloads

### Ministério Público-RJ
* http://apps.mprj.mp.br/sistema/inloco/

### Portal Geo INEA
* https://inea.maps.arcgis.com/apps/MapSeries/index.html?appid=00cc256c620a4393b3d04d2c34acd9ed

### Clima
* https://modis.gsfc.nasa.gov/data/dataprod/mod12.php
* http://earthenginepartners.appspot.com/science-2013-global-forest
* https://www.worldclim.org/
* https://www2.ipef.br/geodatabase/

### Diversas fontes de dados
* https://sosgisbr.com/category/dados-para-download/
* http://datageo.ambiente.sp.gov.br/app/?ctx=DATAGEO#
* https://www.labgis.uerj.br/fontes_dados.php
* http://www.data.rio/
* http://www.inea.rj.gov.br/Portal/MegaDropDown/EstudosePublicacoes/EstadodoAmbiente/index.htm&lang
* http://www.cesadweb.fau.usp.br/index.php?option=com_content&view=article&id=203691&Itemid=1525
* http://www.forest-gis.com/2009/04/base-de-dados-shapefile-do-brasil-todo.html
* http://forest-gis.com/download-de-shapefiles/
* https://www.labtopope.com.br/banco-de-dados-geodesico/

### Uso e ocupação Brasil
http://geo.fbds.org.br/

----

Além disso, o site <https://forest-gis.com/download-gis-base-de-dados/> também apresenta uma coletânea dos principais mapas disponibilizados, com seus respectidos links. 

## Inserir as camadas desejadas no QGIS
Neste capítulo será apresentando um passo a passo para inserir e visualizar as camadas citadas acimea.

### Orientações gerais para todas as camadas
De modo geral, a forma de inserção, reprojetar SRC, recortar área de interesse e definição de propriedades seguem o mesmo roteiro. Por isso, e para evitar repetição de "passo a Passo", será apresentado inicialmente os passos gerais, que se repetem para cada arquivo. Depois, em cada capítulo, serão apresentados os roteiros específicos de acordo com o mapa a ser inserido.

1. Baixar os arquivos - a maioria dos dados/maps são encontrados en sites específicos. nos capítulos específicos de cada tipo de mapa serão apresentados alguns sites onde os dados poderão ser obtidos.
1. A maioria dos mapas poderão ser baixados em formato campactado. Depois do donwload, descompacte e salve todos os arquivos nas pastas/arquivos apropriados, e de caminho conhecido para fácil acesso.
1.Para inserir os dados dos arquivos no projeto, deverão ser seguidos os seguintes passos:
>Primeiro, selecione `Camada` no menu, e depois, `AdicionarCamada` e finalmente, `adicionar camada vetorial` (Figura 2).

![Imagem 2 - Passoas para inserir camada vetorial](../imagens/insesirVetor1.png).

>Na janela que se abre (Figura 3), faze-se necessário escolher o caminho em `base(s) de vetores`, e clicar em `Adicionar` seguido do clique em `close`. A camada será mostrada na janela do Qgis, conforme figura 4.

![Imagem 3 - Passoas para inserir camada vetorial -Janela](../imagens/insereVetor2.png).

![Imagem 4 - Camada inserida na janela do Qgis](../imagens/camada inserida.png).
 
1. Alterar SRC (se necessário). Para a correta funcionalidade da apresentação dos mapas, é fundamental que todos estejam com a mesma referencia geográfica, representada no sistema de referência geogŕafica -`SRC`. Caso o SRC do mapa inserido seja diferente do mapa já existente, ou do SRC do projeto, é necessário alterar o mesmo.  Para verificar o SRC da camada inserida, clique com o botão direito do mouse sobre a camada, na aba **camadas**, e em seguida em **propriedades**. Na janela que se abriu, selecione **informações** no lado esquerdo da janela, e verifique nas informações o SRC. Caso seja diferente do SRC do projeto, siga os passos a seguir. Se for igual, pule os passos seguintes.
>1.1 A) Para alterar o SRC da camada raster, clique em **Raster** no menu principal, depois em **Projeções** e em seguida em **Reprojetar coordenadas**, conforme mostrado na Figura 3.
> B) Para alterar o SRC da camada vetor, clique em **Vetor** no menu principal, depois em **Gerenciar dados** e em seguida selecione **Reprojetar camada**.


![Figura 3 - Reprojetar SRC](../imagens/alterar_SRC_raster.png) 

> 1.1 Na janela da reprojeção, selecione a camada cuja SRC você queira alterar, e os demais campos disponíveis: SRC de origem, SRC de destino, e *sem compactação* conforme mostra a Figura 4.

![Figura 4 - Reprojeção do SRC de camada raster](../imagens/alterar_SRC_raster2.png)   
>1.1 Após essa Reprojeção, aparecerá uma nova camada no Qgis. é importante que você salve esta nova camada em um local conhecido, conforme Figura 5.

![Figura 5 - salvar camada Raster](../imagens/SalvarCamadaRaster.png)

> 1.1 Depois do arquivo estar salvo, você pode remover as camadas rasters/vetor anteriores para evitar confusão, conforme figura 6. 

![Figura 6 - Remover camada não necessária](../imagens/RemoverCamada.png)  

1. Definição das propriedades da nova camada
As propriedades são definidas de acordo com a finalidade e especificidade de cada camada. Por isso, serão apresentadas em cada camada. 

## Divisão política e administrativa
Quando se trabalha com bases georeferencias, a visualizaçao da localização a nível municipal facilita as atividades. Por isso, uma das primeiras camadas a inserir no projeto, é a divisão política e administrativa do estado, em níveil municipal.

A base de dados que pode ser utilizadas é a do IBGE, e pode ser localizada no link <https://www.ibge.gov.br/geociencias/organizacao-do-territorio/malhas-territoriais/15774-malhas.html?=&t=downloads>. Neste link podemos selecionar o estado, e o tipo de nível de divisão que nos interessa e baixar o arquivos em formato .zip.

Depois de inserido a camada, podemos ter a seguinte visão na tela figura 4).
![Figura 4 - Divisão política e administrativa do estado do Paraná, e ao fundo o mapa do StreetMaps](../imagens/div_polit_adm_PAraná.png)

Para recortar o município de interesse podemos seguir os seguintes passos:
1. Clique com o botão direito sobre a camada, na aba *camadas*. Em seguida, clique em *abrir tabela de atributos*. Na janela que se abre, selecione a linha que contém os dados do município de interesse. No exemplo, foi selecionado o município de Palmas. 
2. Feche a janela dos atributos.
3. Na aba *caixa de ferramentas de processamento*, busque por **recortar vetor**. Na lista que aparece, selecione com dois cliques rápidos, *recortar*. Nesta janela, selecione a camada com o mapa do seu estado (Ex.41MUS00G), marque a opção **Apenas feições selecionadas**, tanto na camada de entrada quanto na camada de sobreposição. 4. Finalmente clique em **Executar** e depois em **close**. Se tudo correu bem, foi inserida uma nova camada na aba **camadas**. Desmarque a camada de seu estado, e aparecerá apenas o município recortado.
5. Para salvar esta camada clique com o botão direito do mouse sobre a nova camada (ex. Recortada) e em seguida em *Tornar permanente*. Na nova janela, selecione o nome e caminho onde armazenar a camada (figura 5)

![Figura 5 - Janela para salvar a camada](../imagens/SalvarCamadaMunicípio.png)

Agora, tanto a camada com todos os municípios, quanto a camada com o mapa do município de Palmas podem ser visualizados na janela principal.

## Área dos assentamentos.
O INCRA fornece um conjunto de arquivos shapefile (vetoriais) com a delimitação dos assentamentos do Brasil, com diversas informações úteis <https://certificacao.incra.gov.br/csv_shp/export_shp.py>


Selecione os dados e o estado do seu interesse (Ex. Projetos Assentamentos todos; Paraná). Clinque em enviar, e quando finalizado o processamento dos mpas, clique sobre o arquivo para donwload. 


Para criar uma camada com os assentamentos de interesse, siga os passos 1 a 5 do capítulo anterior. No exemplo, selecionamos os assentamentos São Lourenço, Margem do Iratim e Paraíso do Sul (Figura 7)

![Figura 7 - Assentamentos de interesse](../imagens/Assentamentos de interesse.png)


Com a delimitação da área de interesse, que no nosso caso são os assentamentos, podemos delimitar as demais de acordo com os limites dos assentamentos, facilitando a manipulação, e diminuindo assim consideravelmente o tamanho dos arquivos, como será visto abaixo.

## Dados Ambientais (CAR - Cadastro ambiental rural)
Uma das fontes para obter dados sobre as questões ambientais legais, é o cadastro ambiental rural, que podem ser obtidos no site <http://www.car.gov.br/publico/imoveis/index>. Os dados disponibilizados estão divididos em vários grupos, conforme se visualiza na imagem 1.

![Imagem 1 - Tela com os grupos de informaões do CAR](../imagens/CAR1.png).

### Extrair área de interesse
Ao inserir a camada com algum vetor da base de dados do `CAR`, ela será de todo o município. Quando a área de interesse não for de todo o município, poderão ser extraídos apenas os vetores da área de interesse. Para isso é necessário ter o vetor da área de interesse já defenido, e inserido nas camadas da janela de visualizaao do Qgis.
No nosso caso, a área de interesse é a área total dos assentamentos São Lourenço, Margem do Irati e Paraíso do Sul, cujo vetor tem o nome de `Assentamentos`.


Os cálculos para verificar a área de cada vetor, bem como a porcentagem equevalente, estão no tutorial 4.
Através dos dados do CAR foram elaborados os mapas da imagem 3, que apresenta a localização das áreas de reserva legal, das APP e nascentes dos assentamentos objeto de estudo 

![Imagem 03 - Localização das áreas de reserva ambiental dos assentamentos de Palmas](../imagens/Ambiental_3Ass.png)


## Características do terreno

### Os dados do **Topodata**

A seguir tem uma pequena transcrição sobre os dados do topodata. "O projeto Topodata oferece o Modelo Digital de Elevação (MDE) e suas derivações locais básicas em cobertura nacional, ora elaborados a partir dos dados SRTM disponibilizados pelo USGS na rede mundial de computadores.

Desde que o Topodata foi lançado pela primeira vez, em agosto de 2008, o processamento dos dados foi sucessivamente inspecionado e revisado, com vistas a aprimoramentos e correções. Os dados inicialmente disponibilizados seguiram fielmente as opções e especificações constantes no “Guia de utilização” associado ao Topodata. Porém, problemas na articulação entre folhas e a demanda por mais formatos levaram a um novo tratamento dos dados desde sua preparação, e detalhes do processamento de derivação geomorfométrica foram oportunamente melhorados, e estes novos produtos estiveram disponíveis desde o dia 6 de maio de 2009.

Para possibilitar uma futura expansão do Topodata, foi feita uma nova revisão dos produtos e processos, que culminou numa metodologia passível de aplicação onde quer que existam dados SRTM. Os dados atualmente disponíveis, desde novembro de 2011, foram elaborados em fiel correspondência a estes procedimentos."Fonte <http://www.dsr.inpe.br/topodata/index.php>  

O acesso aos dados - imagens rasters - são através do link abaixo: <http://www.webmapit.com.br/inpe/topodata/>. Neste banco de dados podemos obter dados de: altitude, declividade, orientação, relevo sombreado, com resolução de 30 x 30 m.

## Formato e descrição doss arquivos 
Os arquivos das imagens de declividade, altitude,orientação, relevo e  curvaturas, são em formato disponibilizados em .gif.  
A nomemnclatura segue um padrão, sendo:
Os primeiros dois números representam a latitude da imagem;  
A letra S significa latude sul, e a letra N significa latitude Norte;  
Os três últimos números são da longitude.
As letras finais indicam a finalidade da imagem, sendo:


>> **ZN** para altutude` 

>> **SN** para Declividade`

>> **ON** para Orientação`

>> **OC** para Orientação octante`

>> **RS** para Relevo sombreado`

>> **VN** para Curvatura Vertical de 3 classes`

>> **H3** representa imagens de curvatura Horizontal de 3 classes.`    
  

Os arquvos raster do topodata estão com o SRC EPSG 4674, que é o formato SIRGAS 2000, oficial para a Amárica Latina. Esta informação pode ser obtida, clicando em **propriedades** e depois em **informações**. Quando nosso projeto está sendo elaborado em outro SRC, todos as camadas devem ser reprojetadas para este SRC. No nosso caso, estamos trabalhando com o SRC 31982.


### Recortar a área do vetor Topodata

A estas alturas, você já deve ter delimitado sua área de interesse, conforme já mostramos aqui neste repositório [Veja o documento - Como criar shapefile](CriarShapefilePRV.Rmd).  
Para facilitar os trabalhos, e não sobrecarregar o computador, é recomendável recortar o raster de acordo com a área de interesse, que geralmente é muito menor do que todo o arquivo raster .

Na janela para definir os parâmetros do recorte, fique atendo para selecionar os parâmetros apontados na figura 7.   

![Figura 7 - Extrair raster - passos da definição dos parâmetros](../imagens/RecortarRaster_Camada.png)  

Executado o comando do recorte, aparecerá mais uma camada na tela do QGIS, agora delimitada de acordo com os parâmetros utilizados. A camada recém criada, provavelmente apresentará pontos com altitute igual a zero (em preto). Esses pontos serão removidos durante o processo de salvamento, seguindo os passos conforme figura 8. Veja, que na aba de salvar o arquivo, selecionamos o atributo **nenhum valor de dados** e inserimos os valores a serem desconsiderados na camada salva. No nosso caso, é a altitude zero que deverá se desconsiderada. 

![Figura 8 - Salvar arquivo com SRC reprojetado](../imagens/salvar_raster_reproj.png)  

A camada terá aspectos identicos aos da figura 9.  

![Figura 9 - Imagem do raster recortado](../imagens/RasterAposRecorte.png)

### Configurar a camada para visualização.
A configuração consiste em atribuir cores para as diferentes camadas, de acordo com as minhas necessidades. Existe um "padrão" de paleta de corres, elaboradas pelo INPE, que podem (e devem) ser utilizados para manter o padrão e o profisionalismo do projeto.  
Abaixo cópia de trechos do site <http://blog.webmapit.com.br/2013/02/topodata-paletas-qgis-para-altitude.html> para obtenção e aplicação de palheta de cores para altitudes.  

`Para comemorar os 18 meses em operação do Mapa Índice TOPODATA é com satisfação que disponibilizamos um conjunto de arquivos QML para serem usados no software QGIS com dados do projeto TOPODATA.`

`Estes arquivos definem rampas de cores para o tema Altitude e foram elaborados a partir da adaptação dos valores de elevação e regras de cores contidos nos esquemas de cores SRTM, Terrain, Atlas Shader e ETOPO2 provenientes do software GRASS ( função r.colors ).`

1. Faça o download e extração dos arquivos QML <http://www.webmapit.com.br/downloads/qgis4TopodataBrasil.zip>
2. Na lista de camadas(layers), clique com o botão direito no layer correspondente ao .tif e selecione a opção "Propriedades".  
3. Depois clique no botão "Carregar estilo" e selecione o arquivo .QML correspondente ao estilo que você deseja aplicar e depois clique no botão OK.  Veja a Figura 10.

![Figura 10 - Escolher estilo para cores de altitude](../imagens/escolher_estilo.png)  


Depois de carregado o estilo adequado - no caso de altitude, o estilo **qgis4TopodataBrasil_Etopo2.qml** - é necessário fazer algumas adequações para o local, como por exemplo: Definir o modo de distribuição das cores - *o modo intervalos iguais* parece mais adequados; e o número de classes, conforme demostrado na Figura 11.

![Figura 11 - configurações finais do estilo de palheta de cores](../imagens/definir_caract_estilo.png)   

### A imagem Final   
Após esses passos, você tera finalizado a importação e a configuração da camada de altitude do projeto. Para visualizar individualmente a altitude de cada ponto, basta clicar no ícone **Identificar feições**, e depois no ponto de deseja verificar. As definições deste ponto aparecerão no lado direito, ao alto na janela, conforme demonstrado na figura 12.    

![Figura 12 - Visualização final da Camada de altitude, com as devidas configuraçõẽs com a palheta de cores importada do INPE](../imagens/RasterAltitudeFinal.png)  


# Capacidade de uso do solo 
## segundo LEPSCH et al., 1991
Para o calculo da declividade foi utilizado o arquivo topodata Decliv(B), que apresenta os dados conforme as Classes de capacidade de uso -definem o grau de limitação do uso (I, II, III, IV, V, VI, VII e VIII): 

Classe I: terras cultiváveis, aparentemente sem problemas especiais de conservação; 

Classe II: terras cultiváveis com problemas simples de conservação; 

Classe III: terras cultiváveis com problemas complexos de conservação; 

Classe  IV: terras  cultiváveis  apenas  ocasionalmente  ou  em  extensão  limitada,  com sérios problemas de conservação; 

Classe   V: terras   adaptadas   em   geral   para   pastagens   e/ou   reflorestamento,   sem necessidade  de  práticas  especiais  de  conservação,  cultiváveis  apenas  em  casos  muito especiais; 

Classe  VIII: terras  impróprias  para  cultura,  pastagem  ou  reflorestamento,  podendo servir  apenas  como  abrigo  e  proteção  da  fauna  e  flora  silvestre,  como  ambiente  para recreação, ou para fins de armazenamento de água (LEPSCH et al., 1991).

### Buscando os dados
```{r message = FALSE, echo = TRUE, comment=FALSE, warning=FALSE}
require(readr)
Declive_B<- read_delim("data/declive_b.txt",
                              "|",
                              escape_double = FALSE, 
                              col_names = FALSE, 
                              trim_ws = TRUE,
                              skip = 4)
View(Declive_B)
```


### organizando dados
```{r, include=TRUE}

Declive_b<-Declive_B%>%
  filter(!is.na(X4))%>%
  select(X2,X4)%>%
  transmute(
    Classe=X2,
    Metro_Quad=X4,
    Ha=X4/10000, 
    Porcentagem=X4/sum(X4)*100,
  )%>%
  arrange(Classe)

head(Declive_b)
```


### A tabela com os dados
  
```{r kable_b_dados declive}

kable(Declive_b, 
      format = "simple", 
      digits = 1,
      padding = 2,
      caption = "1 - Quantidade de metros quadrados de área de cada classe de declividade, no assentamento Sâo Lourenço")
```
  
 
## Segundo Embrapa 1999

Classe nº | Classe de relevo | Declividade (%)
--------- | ---------------- | -------------------
1 | Plano |0 – 3
2 | Suave Ondulado | 3 – 8
3 | Ondulado | 8 – 20
4 | Forte Ondulado | 20 – 45
5 | Montanhoso | 45 – 75
6 | Escarpado | >   75
	

### Buscando os dados
```{r message = FALSE, echo = TRUE, comment=FALSE, warning=FALSE}

Declive_C<- read_delim("data/declive_c.txt",
                              "|",
                              escape_double = FALSE, 
                              col_names = FALSE, 
                              trim_ws = TRUE,
                              skip = 4)
View(Declive_C)
```


### organizando dados
```{r, include=TRUE}

Declive_c<-Declive_C%>%
  filter(!is.na(X4))%>%
  select(X2,X4)%>%
  transmute(
    Classe=X2,
    Metro_Quad=X4,
    Ha=X4/10000, 
    Porcentagem=X4/sum(X4)*100,
  )%>%
  arrange(Classe)

#head(Declive_c)
```


### A tabela com os dados
  
```{r}

kable(Declive_c, 
      format = "simple", 
      digits = 1,
      padding = 2,
      caption = "1 - Quantidade de metros quadrados de área de cada classe de declividade, conforme classificação da Embrapa (1991) no assentamento Sâo Lourenço")
```
  

----
# Impressão dos mapas
No processo de elaboração dos mapas do Assentamento São Lourenço, foram inseridos os rasters de altitude e orientação do terreno, conforme roteiro apresentado acima.  
A inserção desses dois rasters gera dois mapas distintos, já que não é possível mesclar os dois mapas numa só visialização (ou pelo menos não tem funcionalidades).  

### Adicionando itens ao layout
A impressão dos mapas, em formato vários formatos, pode ser feito através do `layout de impressão`, que pode ser acessado através do menu `Projeto`.  
> O primeiro passa é atribuir um nome ao nome do layout (Figura 13). Lembre-se que para o mesmo projeto podem haver vários layouts.  

![Figura 13 - Promt para atribuir nome ao novo layout ](../imagens/tituloLayout.png)  

>>** Não nos preocupamos neste momento com as configurações de cada adição. Isso será tratado no próximo capítulo.**

> O segundo passo, é desenhar (demarcar) as mimensões do mapa dentro da "folha", clicando no botão `Adicionar mapa`, conforme marcado com o ńumero 1 na figura 14.

![Figura 14 - Página nova do layout de impressão](../imagens/LayotPagNova.png)  

> O terceiro passo, é o ajuste do mapa dentro da área demarcada. Para ajustar o zoom ou deslocar a área interna do mapa, clica-se no botão `mover o conteúdo do item`. Para ajustar o item (a demarcação), clica-se no botão `selecionar/mover item`, conforme demonstrado na figura 6.

> O quarto passo é a inserção de Barra de escala e seta norte, se for o caso. Para inserir a barra de escala, clica-se no botão `Inserir Barra Escala`, e para inserir a seta norte, clica-se no botão `Inserir Seta Norte`, conforme figura 14.
> O quinto passo é a inserção do Título do mapa, que será feito clicando no botão `Adicionar Rótulo`.
> O sexto passo será inclusão da Legenda, clicando no botão `Adicionar Legenda`.

### Configuração dos itens adicionados  
Depois de adicionados os itens, deve-se dedicar um tempo para sua edição, conforme será visto a seguir. Basicamente, as edições são reallizadas nas ferramentas dispostas no lado direito da tela do layout (Figura 15). Na parte superior vê-se a janela dos itens adicionados, e do meio para a parte inferior, a parte dos modelos e das propriedades de cada item. Ao selecionar um item, as propriedades do mesmo aprecem nesta parte, e é aí que se fazem as configurações.    
Na aba do item, pode-se selecionar os que aparecerão na impressão, e bloquear a edição do item. Para bloquear a edição do conteúdo do interior do item, é preciso acessar a aba abaixo, de propriedades do item.

![Figura 15 - Janela do layout de impressão, com destaque para a área de configurações dos itens](../imagens/LayoutConfigura.png)  

### Configurações do Mapa  
O primeiro item adicionado, também poderá ser o primeiro a ser configurado. Ao clicar no item `mapa 1` e selecionar a aba `propriedades do item`, são mostradas as configurações possíveis, como: Escala, SRC, rotação, travar camadas, travar estilo, grades, enquadramento, posição e tamanho, moldura, etc. 
Neste exemplo, iremos incluir uma moldura, e travar as camadas. **Depois de travar as camadas, posso incluir outro mapa, com outro tema, sem prejuízos ao mapa atual**.

### Configurações da legenda
Ao inclui a legenda, aparece a legenda de todas as camadas existentes no arquivo do projeto, o que geralmente não é o desejável.  
Pode-se atribuir um Título ao mapa, com as configurações necessárias do título do mapa. No exemplo foi adicionado o título *Legenda*. 
Em seguida, é necessário selecionar a qual mapa a legenda se relaciona (neste caso, ao mapa 1).  
Em relação ao aranjo, selecionamos *simbolos à esquerda* (Figura 16).  

![Figura 16 - Primeira parte da configuração da legenda](../imagens/LayoutConfLegenda.png)  

Em relação aos itens da legenda, temos a modificação mais importante e substancial a ser feita. Aqui vamos selecionar e renomear (se necessário) aquilo que irá aparecer na impressão final.
Se não queremos que apareça tudo o que estiver aparecendo até agora na legenda, deslececiona-se o "combo" `Atualização automática` (Figura 17).
Em seguida, seleciona-se o item que será modificado, ou excluído. No exemplo, serão mantidos apenas os itens `app_Assenta_Car, Hrido_SL e Assentamento_SL`. que serão ainda editados. Para excluir os demais itens, os mesmos foram selecionados, e em seguida foi clicado no botão **-** na parte inferior da janela. Para editar (renomear) os itens que permanecem na legenda, basta clicar no mesmo, e em seguida no botão *editar* na parte inferior da janela. 

![Figura 17 - Segunda parte da configuração da legenda](../imagens/LayoutConfLegenda2.png)  

### Configuração da barra de escala
A barra de escala geralmente também necessita de alterações, pois suas configurações depende do tamanho da área do mapa.  O primeiro passo e certificar-se de que está relacionada ao mapa correto, neste caso, com o mapa 1. A escolha do estilo é muito próprio de cada um, e no exemplo foi selecionado o estilo de `linhas tracejadas do meio`.   
Em relação às unidades, podemos aqui trabalhar com metros, ou km. Foi escolhido a opção em Quilômetros, por ser mais adeuqada, mas a escolha depende de cada caso em específico. O mesmo é recomendado para as demais especificações. No exemplo, definiu-se por 2 segmentos à direita, a divisão em 0,5 unidades (0,5 km) (Figura 18). 

![Figura 18 - Seleção das especificidades da barra de escala](../imagens/Layt_Esc_Seg.png).  

### Configurações do Título
A última configiração necessária para este mapa, é a fonte do título. Atribuimos fonte 12, além de fazer o alinhamento necessário para corresponder ao tamanho do mapa. 

### Impressão do Mapa
Para imprimir o mapa, temos a opção de imprimir em impressora, ou em formato pdf, SVG ou png. A opção, para inserir o impresso neste script, foi `APP impressão.png` (Figura 19) com resolução de 100 dpi (para não pesar muito).

![Figura 19 - Impressão do mapa eleaborado conforme roteiro descrito no script](../imagens/APP impressao.png)


